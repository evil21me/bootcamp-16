package object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static ArrayList<User> users = new ArrayList<>();


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println(" Count of users: ");
        int count = scanner.nextInt();

        for (int i = 0; i < count; i++) {
            String firstName = scanner.next();
            String lastName = scanner.next();
            Integer age = scanner.nextInt();
            users.add(new User(firstName, age, lastName));
        }

        System.out.println(users);
    }
}
