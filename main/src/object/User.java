package object;

public class User {
    public String firstName;
    public String lastName;
    public Integer age;

    public User(String firstName, Integer age, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }


    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
