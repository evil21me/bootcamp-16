package mobile;

public class Application {
    public String name;
    public Integer storage;

    public Application(String name, Integer storage) {
        this.name = name;
        this.storage = storage;
    }

    @Override
    public String toString() {
        return "Application{" +
                "name='" + name + '\'' +
                ", storage=" + storage +
                '}';
    }
}
