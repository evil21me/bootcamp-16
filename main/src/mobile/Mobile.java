package mobile;

import java.util.ArrayList;
import java.util.Scanner;

public class Mobile {
    public String company;
    public String model;
    public Integer memory;
    public ArrayList<Application> apps = new ArrayList<>();

    public Mobile(String company, String model, Integer memory) {
        this.company = company;
        this.model = model;
        this.memory = memory;
    }

    public Mobile() {

    }


    public Mobile(String company, String model, Integer memory, ArrayList<Application> apps) {
        this.company = company;
        this.model = model;
        this.memory = memory;
        this.apps = apps;
    }


    public void addApp() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Application name: ");
        String appName = scanner.next();

        System.out.println("Application name: ");
        Integer storage = scanner.nextInt();

        if (isEnoughSpace(storage)) {
            Application newApp = new Application(appName, storage);
            apps.add(newApp);
        } else {
            System.out.println("There is no enough space!");
        }
    }

    public boolean isEnoughSpace(Integer newAppStorage) {
        Integer totalAppStorage = 0;
        for (Application app : apps) {
            totalAppStorage += app.storage;
        }
        return memory > totalAppStorage + newAppStorage;
    }

    @Override
    public String toString() {
        return "Mobile{" +
                "company='" + company + '\'' +
                ", model='" + model + '\'' +
                ", memory=" + memory +
                ", apps=" + apps +
                '}';
    }
}
