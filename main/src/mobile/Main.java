package mobile;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Application> iphoneApps = new ArrayList<>();

        Application facebook = new Application("Facebook", 1);
        Application instagram = new Application("Instagram", 1);

        iphoneApps.add(instagram);
        iphoneApps.add(facebook);

        Mobile iphone = new Mobile("Apple", "Iphone XS Max", 256, iphoneApps);

        System.out.println("Agar app qo'shishni xohlasangiz 1 ni bosing aka");
        Scanner scan = new Scanner(System.in);
        int command = scan.nextInt();

        if (command == 1) {
            iphone.addApp();
        }
        System.out.println(iphone);
    }
}
