package encapsulation;

public class Car {
    private String brand;
    private String model;

    private String color;
    private String type;

    public Car(String brand, String model, String color, String type) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        setType(type);
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        if (type.equals("TAXI") && color.equals("YELLOW")) {
            this.color = color;
        }
        if (!type.equals("TAXI")) {
            this.color = color;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if ("TAXI".equals(type)) {
            this.type = type;
            this.color = "YELLOW";
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
