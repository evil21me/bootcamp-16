package encapsulation;

public class Main {
    public static void main(String[] args) {
        Car nexia = new Car("GM", "Nexia R3", "White", "TAXI");
//        nexia.setType("TAXI");
//        nexia.setColor("Black");
        System.out.println(nexia);
    }
}
